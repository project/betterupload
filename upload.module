<?php

/**
 * @file
 * General File-handling API
 */

/**
 * General functions, help, menu, perms, settings etc
 */
/**
 * Implementation of hook_help().
 */
function upload_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      return t('Allows users to upload and attach files to content.');
    case 'admin/settings/upload':
      return t('<p>Users with the <a href="%permissions">upload files permission</a> can upload attachments. You can choose which post types can take attachments on the <a href="%types">content types settings</a> page.</p>', array('%permissions' => url('admin/access'), '%types' => url('admin/settings/content-types')));
  }
}

/**
 * Implementation of hook_perm().
 */
function upload_perm() {
  return array('upload files');
}

/**
 * Implementation of hook_menu().
 */
function upload_menu($may_cache) {
  $items = array();

  if ($may_cache) {
    $items[] = array(
      'path' => 'upload/js',
      'callback' => 'upload_js',
      'access' => user_access('upload files'),
      'type' => MENU_CALLBACK
    );
  }
  return $items;
}

/**
 * Implementation of hook_settings().
 */
function upload_settings() {

  $form['settings_general'] = array('#type' => 'fieldset', '#title' => t('General settings'));

  $roles = user_roles(0, 'upload files');

  foreach ($roles as $rid => $role) {
    $form["settings_role_$rid"] = array('#type' => 'fieldset', '#title' => t('Settings for %role', array('%role' => theme('placeholder', $role))), '#collapsible' => TRUE, '#collapsed' => TRUE);
    $form["settings_role_$rid"]["upload_extensions_$rid"] = array(
      '#type' => 'textfield', '#title' => t('Permitted file extensions'), '#default_value' => variable_get("upload_extensions_$rid", "jpg jpeg gif png txt html doc xls pdf ppt pps"),
      '#size' => 60, '#maxlength' => 255, '#description' => t('Extensions that users in this role can upload. Separate extensions with a space and do not include the leading dot.')
    );
    $form["settings_role_$rid"]["upload_uploadsize_$rid"] = array(
      '#type' => 'textfield', '#title' => t('Maximum file size per upload'), '#default_value' => variable_get("upload_uploadsize_$rid", 1),
      '#size' => 5, '#maxlength' => 5, '#description' => t('The maximum size of a file a user can upload (in megabytes).')
    );
    $form["settings_role_$rid"]["upload_usersize_$rid"] = array(
      '#type' => 'textfield', '#title' => t('Total file size per user'), '#default_value' => variable_get("upload_usersize_$rid", 10),
      '#size' => 5, '#maxlength' => 5, '#description' => t('The maximum size of all files a user can have on the site (in megabytes).')
    );
  }

  $extra_form = uplaod_invoke_fileapi(&$form, 'settings');
  $form = array_merge($form, $extra_form);
  return $form;
}

/**
 * File API functions
 */

/**
 * Invoke a hook_fileapi() operation in all modules.
 *
 * @param &$file
 *   A file object.
 * @param $op
 *   A string containing the name of the fileapi operation.
 * @param $a3, $a4
 *   Arguments to pass on to the hook, after the $node and $op arguments.
 * @return
 *   The returned value of the invoked hooks.
 */
function upload_invoke_fileapi(&$file, $op, $a3 = NULL, $a4 = NULL) {
  $return = array();
  foreach (module_implements('fileapi') as $name) {
    $function = $name .'_fileapi';
    $return_file = $function($node, $op, $a3, $a4);
  }
  return $return_file;
}

/**
 * Insert a file in the database and the storage system
 *
 * @param $files
 *   An array with files objects.
 * @return
 *   An array with successfully stored files.
 */
function upload_save($files) {
  foreach ($files as $key => $file) {
    $file = upload_invoke_fileapi($file, 'save');
    if ($file->source) {
      // Clean up the session:
      unset($_SESSION['file_uploads'][$file->source]);
       
      // Insert new files:
      if ($file = drupal_file_save($file, $file->filename)) {
        $fid = db_next_id('{files}_fid');
        db_query("INSERT INTO {files} (fid, obid, vid, filename, filepath, filemime, filesize, list, data) VALUES (%d, %d, %d, '%s', '%s', '%s', %d, %d)",
                 $fid, $node->nid, $node->vid, $file->filename, $file->filepath, $file->filemime, $file->filesize, $node->list[$key]);
      }
    }
  }
  return;
}

/**
 * Delete a file from the database and the disk.
 *
 * @param $files
 *   An array with files objects.
 * @return Nothing
 *
 */
function upload_delete($files) {
  foreach ($files as $file) {
    file_delete($file->filepath);
  }
  db_query("DELETE FROM {files} WHERE obid = %d", $file->obid);
}

/**
 * Delete a file from the database and the disk.
 *
 * @param $files
 *   An array with files objects.
 * @return $files
 *   An array with files objects.
 */
function upload_validate($files) {
  //build the file object
  foreach ($files as $file) {
    if ($file = file_check_upload($key)) {
      $valid_files[$file->source] = $file;
      if ($file->source) {
        $filesize += $file->filesize;
      }
    }
  }
  $files = $valid_files;
  $valid_files = array();

  //Check for a temporary file that was just uploaded 
  if (($file = file_check_upload()) && user_access('upload files')) {
    global $user;

    // Don't do any checks for uid #1.
    if ($user->uid != 1) {
      // Validate file against all users roles. Only denies an upload when
      // all roles prevent it.
      $total_usersize = upload_space_used($user->uid) + $filesize;
      foreach ($user->roles as $rid => $name) {
        $extensions = variable_get("upload_extensions_$rid", 'jpg jpeg gif png txt html pdf odt ods odp odg odc odf odb odi odm svg tar gz gzip bz bzip mp3 ogg flac');
        $uploadsize = variable_get("upload_uploadsize_$rid", 1) * 1024 * 1024;
        $usersize = variable_get("upload_usersize_$rid", 1) * 1024 * 1024;

        $regex = '/\.('. ereg_replace(' +', '|', preg_quote($extensions)) .')$/i';

        if (!preg_match($regex, $file->filename)) {
          $error['extension']++;
        }

        if ($uploadsize && $file->filesize > $uploadsize) {
          $error['uploadsize']++;
        }

        if ($usersize && $total_usersize + $file->filesize > $usersize) {
          $error['usersize']++;
        }
      }
    }

    // Rename possibly executable scripts to prevent accidental execution.
    // Uploaded files are attachments and should be shown in their original
    // form, rather than run.
    if (preg_match('/\.(php|pl|py|cgi|asp)$/i', $file->filename)) {
      $file->filename .= '.txt';
      $file->filemime = 'text/plain';
    }

    if ($error['extension'] == count($user->roles) && $user->uid != 1) {
      form_set_error('upload', t('The selected file %name can not be attached to this post, because it is only possible to attach files with the following extensions: %files-allowed.', array('%name' => theme('placeholder', $file->filename), '%files-allowed' => theme('placeholder', $extensions))));
    }
    elseif ($error['uploadsize'] == count($user->roles) && $user->uid != 1) {
      form_set_error('upload', t('The selected file %name can not be attached to this post, because it exceeded the maximum filesize of %maxsize.', array('%name' => theme('placeholder', $file->filename), '%maxsize' => theme('placeholder', format_size($uploadsize)))));
    }
    elseif ($error['usersize'] == count($user->roles) && $user->uid != 1) {
      form_set_error('upload', t('The selected file %name can not be attached to this post, because the disk quota of %quota has been reached.', array('%name' => theme('placeholder', $file->filename), '%quota' => theme('placeholder', format_size($usersize)))));
    }
    else {
      $key = 'upload_'. count($_SESSION['file_uploads']);
      $file->source = $key;
      $file = file_save_upload($file);
      $valid_files[$key] = $file;
    }
  }
  for ($x = 0; $x < count($_SESSION['file_uploads']); $x++) {
    $key = 'upload_' . $x;
    if ($file = file_check_upload($key)) {
      $valid_files[$key] = $file;
    }
  }
  return $files;
}

/**
 * Delete a file from the database and the disk.
 *
 * @param $vid
 *   An integer for the revision ID
 * @return $files
 *   An array with files objects.
 */
function upload_load($vid) {
  $files = array();

  if ($vid) {
    $result = db_query("SELECT * FROM {files} WHERE vid = %d", $vid);
    while ($file = db_fetch_object($result)) {
      $files[$file->fid] = $file;
    }
  }

  return $files;
}

/**
 * Renders a basic file form
 *
 * @param $files
 *   An array with files objects.
 * @param $object
 *   The object, if available, where th file belongs to. for example $user or $node
 * @return
 *   the form array
 */

function upload_form($files, $object) {
  drupal_add_js('misc/progress.js');
  drupal_add_js('misc/upload.js');

  $form = upload_invoke_fileapi($file, 'form');

  return $form;
}

/**
 * Determine how much disk space is occupied by a user's uploaded files.
 *
 * @param $uid
 *   The integer user id of a user.
 * @return
 *   The ammount of disk space used by the user in bytes.
 */
function upload_space_used($uid) {
  return db_result(db_query('SELECT SUM(f.filesize) FROM {files} f INNER JOIN {node_revisions} n ON f.vid = n.vid WHERE uid = %d', $uid));
}

/**
 * Determine how much disk space is occupied by uploaded files.
 *
 * @return
 *   The ammount of disk space used by uploaded files in bytes.
 */
function upload_total_space_used() {
  return db_result(db_query('SELECT SUM(f.filesize) FROM {files} f INNER JOIN {node_revisions} n ON f.vid = n.vid'));
}

/**
 * Helper functions
 */

