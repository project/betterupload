Description:
------------
We have three layers from bottom to top they are:
 Toolboxes
 File APIs
 Upload APIs

Toolboxes - file.protocol.description.inc
Interesting for those that want fancy storage methods.
An example protocol is http for 'storage' on a remote website. A toolbox will make sure that the correct path/url is generated (http://example.com/file.png). Another example is storage in a dir per user. A toolbox will then simply replace the default 'file' storage toolbox (file.file.peruserdir.inc).

File APIs - file.inc
Normally one does not need to bother about this, zhen creating upload and attachement modules. It is only there for lower level filehandling, like uploading a logo to your system. It contains a set of file functions that can be called (similar to file.inc we have now)

Upload APIs - upload.module
This is formed after nodapi. It contains basic, global settings, a set of hooks and handles database storage. There is a whole range of 'operations' like 'save', 'delete' but also 'settings' and 'form'. Most modules should use this. upload.module allows attaching files to ANY object, nodes, terms, comments, users etc.
An example module using these apis is attachement.module, which allows us to attach files to nodes.

Difflog:
Because we want to keep this as close to core please add your changes and differneces with core in this list:
Ber: Add inline functionality: http://drupal.org/node/26288
Ber: changed the allowed default file types: jpg jpeg gif png txt html pdf odt ods odp odg odc odf odb odi odm svg tar gz gzip bz bzip
Ber: splitted the modules into a display and an upload/handling partFilesystem in a nutshell


Filesystem in a nutshell

We have three layers from bottom to top they are:
 Toolboxes
 File APIs
 Upload APIs

Toolboxes - file.protocol.description.inc
Interesting for those that want fancy storage methods.
An example protocol is http for 'storage' on a remote website. A toolbox will make sure that the correct path/url is generated (http://example.com/file.png). Another example is storage in a dir per user. A toolbox will then simply replace the default 'file' storage toolbox (file.file.peruserdir.inc).

File APIs - file.inc
Normally one does not need to bother about this, zhen creating upload and attachement modules. It is only there for lower level filehandling, like uploading a logo to your system. It contains a set of file functions that can be called (similar to file.inc we have now)

Upload APIs - upload.module
This is formed after nodapi. It contains basic, global settings, a set of hooks and handles database storage. There is a whole range of 'operations' like 'save', 'delete' but also 'settings' and 'form'. Most modules should use this. upload.module allows attaching files to ANY object, nodes, terms, comments, users etc.
An example module using these apis is attachement.module, which allows us to attach files to nodes.
